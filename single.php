<?php get_header(); ?>

    <!-- breadcrumb area start -->
    <section class="banner_area">
        <div class="container">
            <div class="banner_text">
                <h3>Blog</h3>
                <ul>
                    <li><a href="<?php echo home_url() ?>">Home</a></li>
                    <li><a href="#"><?php echo the_title() ?></a></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- breadcrumb area end -->
    

    <?php while ( have_posts() ) : the_post(); ?>
    <!-- product details wrapper start -->
    
    <!--================Blog Main Area =================-->
    <section class="main_blog_area p_100">
        <div class="container">
            <div class="row blog_area_inner">
                <div class="col-lg-9">
                    <div class="main_blog_inner single_blog_inner">
                        <div class="blog_item">
                            <div class="blog_img">
                                <?php echo the_post_thumbnail('shop_single', array('class' => 'img-fluid')); ?>
                            </div>
                            <div class="blog_text">
                                <div class="blog_time">
                                    <div class="float-left">
                                        <a href="#"><?php echo the_date( 'l, F j, Y'); ?></a>
                                    </div>
                                    
                                </div>
                                <a href="#">
                                    <h4><?php echo the_title() ?></h4>
                                </a>
                                <p><?php echo the_content('full') ?> </p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="right_sidebar_area">
                        
                        <aside class="r_widget recent_widget">
                            <div class="r_title">
                                <h3>Recent News</h3>
                            </div>
                            <div class="recent_w_inner">

                                <?php 
                                    $query = new WP_Query( array('post_type' => 'post') ); 
                        
                                    while($query->have_posts()): $query->the_post(); ?>

                                    <div class="media">
                                        <div class="d-flex">
                                            <?php echo the_post_thumbnail('thumbnail', array('class' => 'img-fluid recent-icon')); ?>
                                        </div>
                                        <div class="media-body">
                                            <a href="<?php echo the_permalink(); ?>">
                                                <h4><?php echo the_title(); ?></h4>
                                            </a>
                                            <a href="#">
                                                <p>08 Feb 2018</p>
                                            </a>
                                        </div>
                                    </div>

                                    <?php endwhile; ?>
                            </div>
                        </aside>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    

<?php endwhile; ?>
    

<?php get_footer(); ?>
