<?php get_header() ?>

<div class="page-content">
    <?php while ( have_posts() ) : the_post(); ?>
        <?php 	echo the_content(); ?>
    <?php endwhile; ?>
</div>
<?php get_footer() ?>