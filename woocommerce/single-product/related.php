<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

</div>

	<section class="similar_product_area p_100">
	<div class="container">

		<?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Similar products', 'woocommerce' ) );

		if ( $heading ) : ?>
			<div class="main_title"><h2><?php echo esc_html( $heading ); ?></h2></div>
		<?php endif; ?>
		
		<?php woocommerce_product_loop_start(); ?>
			<?php foreach ( $related_products as $related_product ) : ?>
					<?php
					$post_object = get_post( $related_product->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found
					?>
					<div <?php wc_product_class( 'col-lg-3 col-md-3 col-6', $product ); ?>>
						<div class="cake_feature_item">
							<div class="cake_img">
								<?php echo the_post_thumbnail('shop-thumb'); ?>
							</div>
							<div class="cake_text">
							<?php
								do_action( 'woocommerce_before_shop_loop_item' );
								echo '<h3>'.get_the_title().'</h3>';
								do_action( 'woocommerce_after_shop_loop_item_title' );
								do_action( 'woocommerce_after_shop_loop_item' );
								?>
							</div>
						</div>
					</div>
			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>
		</div>
		</div>

	</section>

	<div class="container">

	<?php
endif;

wp_reset_postdata();
