<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>
<section class="banner_area">
        <div class="container">
            <div class="banner_text">
                <h3>Shop</h3>
                <ul>
                    <li><a href="<?php echo home_url() ?>">Home</a></li>
                    <li><a href="#">Shop</a></li>
                </ul>
            </div>
        </div>
	</section>
	


<section class="product_area p_100">
        <div class="container">
            <div class="row product_inner_row">

	<div class="col-lg-9">
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
?></div>
<div class="col-lg-3"> 

	<div class="p_w_title">
        <h3>Product Categories</h3>
	</div>
	<aside class="left_sidebar p_catgories_widget">
        <ul class="list_style">
			<?php wp_list_categories( array('taxonomy' => 'product_cat', 'title_li'  => '', 'show_count'=> 1,) ); ?>
		</ul>
	</aside>

	<aside class="left_sidebar p_sale_widget">
        <div class="p_w_title">
            <h3>Top Sale Products</h3>
		</div>
		
		<?php
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 6
				);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
				global $product;
					?>
					<div class="media">
						<div class="d-flex">
							<?php echo the_post_thumbnail('sidebar-thumb'); ?>
						</div>
						<div class="media-body">
							<a href="<?php echo get_the_permalink() ?>">
								<h4><?php echo get_the_title() ?></h4>
							</a>
							<h5><?php echo $product->get_price_html();?></h5>
						</div>
					</div>
					<?php 
				endwhile;
				}
				wp_reset_postdata();
			?>
	</aside>
						

</div> 

</div>
</div> 
			</section> <?php

get_footer();

// get_footer( 'shop' );
