<?php get_header() ?>
    <!--================End Main Header Area =================-->

    <section class="main_slider_area">
        <div id="main_slider3" class="rev_slider" data-version="5.3.1.6">
            <ul>
                <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php echo get_template_directory_uri() ?>/img/home-slider/slider-6.jpg" data-rotate="0" data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo get_template_directory_uri() ?>/img/home-slider/main-slider01.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box">
                        <div class="tp-caption tp-resizeme first_text black" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['220','220','170','170','130']" data-fontsize="['65','65','65','40','30']" data-lineheight="['80','80','80','50','40']" data-width="['800','800','800','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','left']">Moroccan & <br /> Emirati Cuisine</div>

                        <div class="tp-caption tp-resizeme secand_text black" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['388','388','340','290','225']" data-fontsize="['20','20','20','20','16']" data-lineheight="['28','28','28','28']" data-width="['640','640','640','640','350']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit quia consequuntur magni dolores eos qui ratione
                        </div>

                        <div class="tp-caption tp-resizeme slider_button" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['485','485','430','390','330']" data-fontsize="['14','14','14','14']" data-lineheight="['46','46','46','46']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                            <a class="now_btn" href="#">View Our Menu</a>
                        </div>
                    </div>
                </li>
                <li data-index="rs-1588" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php echo get_template_directory_uri() ?>/img/home-slider/slider-7.jpg" data-rotate="0" data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo get_template_directory_uri() ?>/img/home-slider/slider-7.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box">
                        <div class="tp-caption tp-resizeme first_text" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['220','220','170','170','130']" data-fontsize="['65','65','65','40','30']" data-lineheight="['80','80','80','50','40']" data-width="['800','800','800','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','left']">Cake Bakery ... <br /> make delicious products</div>

                        <div class="tp-caption tp-resizeme secand_text" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['388','388','340','290','225']" data-fontsize="['20','20','20','20','16']" data-lineheight="['28','28','28','28']" data-width="['640','640','640','640','350']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit quia consequuntur magni dolores eos qui ratione
                        </div>

                        <div class="tp-caption tp-resizeme slider_button" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['485','485','430','390','330']" data-fontsize="['14','14','14','14']" data-lineheight="['46','46','46','46']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                            <a class="now_btn" href="#">Purchase now</a>
                        </div>
                    </div>
                </li>
                <li data-index="rs-1589" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php echo get_template_directory_uri() ?>/img/home-slider/slider-8.jpg" data-rotate="0" data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo get_template_directory_uri() ?>/img/home-slider/slider-8.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box">
                        <div class="tp-caption tp-resizeme first_text" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['220','220','170','170','130']" data-fontsize="['65','65','65','40','30']" data-lineheight="['80','80','80','50','40']" data-width="['800','800','800','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','left']">Cake theme ... <br /> made with care and love</div>

                        <div class="tp-caption tp-resizeme secand_text" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['388','388','340','290','225']" data-fontsize="['20','20','20','20','16']" data-lineheight="['28','28','28','28']" data-width="['640','640','640','640','350']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit quia consequuntur magni dolores eos qui ratione
                        </div>

                        <div class="tp-caption tp-resizeme slider_button" data-x="['left','left','left','15','15']" data-hoffset="['0','15','15','0']" data-y="['top','top','top','top']" data-voffset="['485','485','430','390','330']" data-fontsize="['14','14','14','14']" data-lineheight="['46','46','46','46']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                            <a class="now_btn" href="#">Purchase now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <!--================End Slider Area =================-->

    <section class="offer-banner pt_100">

        <div class="container">

            <div class="row our_bakery_image">
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/featured01.jpg" alt="">
                </div>
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/featured02.jpg" alt="">
                </div>
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/featured03.jpg" alt="">
                </div>
            </div>

        </div>



    </section>



    <!--================Our Bakery Area =================-->
    <section class="our_bakery_area p_100">
        <div class="container">
            <div class="our_bakery_text">
                <h2>OUR MENU</h2>
                <h6>Before you pay us a visit, we encourage you to take a look at our menu. Each and every day,<br> we strive to bring new dishes to the table for our customers, <br> with exciting flavors and aromas that are inspired from the cuisines native to our sources.</h6>

            </div>
            <div class="row our_bakery_image">

                <!-- product starting -->
                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/breakfast.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3>Breakfast</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/breakfast"></a>
                    </figure>

                </div>


                <!-- product starting -->



                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/main-course.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3>Main Course</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/main-course"></a>
                    </figure>

                </div>


                <!-- product starting -->


                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/cake-feature/product01.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3>Sandwiches</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/sandwiches"></a>
                    </figure>

                </div>


                <!-- product starting -->



                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/cake-feature/product01.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3>Desserts</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/desserts"></a>
                    </figure>

                </div>


                <!-- product starting -->


                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/soft-drink.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3> Cold Beverages</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/hot-cold-beverages/"></a>
                    </figure>

                </div>



                <div class="col-lg-4 col-sm-6 mb-40">
                    <figure class="snip1529">
                        <img src="<?php echo get_template_directory_uri() ?>/img/tea.jpg" alt="Gazania Cafe" />
                        <div class="menu"><span class="menu-bold">MENU</span></div>
                        <figcaption>
                            <h3>Special Coffee</h3>

                        </figcaption>
                        <div class="hover"><i class="ion-android-open"></i></div>
                        <a href="<?php echo home_url()?>/special-coffee"></a>
                    </figure>

                </div>




            </div>



        </div>
    </section>
    <!--================End Our Bakery Area =================-->

    <!--================Welcome Area =================-->
    <section class="welcome_bakery_area pink_cake_feature">
        <div class="container">
            <div class="cake_feature_inner">
                <div class="title_view_all">
                    <div class="float-left">
                        <div class="main_w_title">
                            <h2>Our Featured Cakes</h2>
                            <h5> Seldolor sit amet consect etur</h5>
                        </div>
                    </div>
                    <div class="float-right">
                        <a class="pest_btn" href="#">View all Products</a>
                    </div>
                </div>
                <div class="cake_feature_slider owl-carousel">

                <?php
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 6
				);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
				global $product;
                    ?>

					<div class="item">
						<div class="cake_feature_item">
                            <div class="cake_img">
                                <?php echo the_post_thumbnail('shop-thumb'); ?>
                            </div>
						<div class="cake_text">
                            <h4>AED<?php echo $product->get_price();?></h4>
                            <h3><?php echo get_the_title() ?></h3>
                            <a class="pest_btn" href="<?php echo get_the_permalink() ?>">View product</a>
                        </div>
						</div>
                        
					</div>
					<?php 
				endwhile;
				}
				wp_reset_postdata();
            ?>
            


            </div>
        </div>
    </section>
    <!--================End Service We offer Area =================-->


    <!--================Our Bakery Area =================-->
    <section class="our_bakery_area p_100">
        <div class="container">
            <div class="our_bakery_text">
                <h2>Dine With Class </h2>
                <h6>Food is an inevitable part of our lives, so as a fine dining experience. We believe that the food will be two times tastier when is it served in the right way in a right arrangement.</h6>
                <p>Gazaniacafe is bringing you three world-class dining places inspired by modern Emirati and Moroccan hospitality. Now you can taste our international delicious cuisine in the best ever dining area in the heart of Dubai. It will be a memorable food experience for sure.</p>
            </div>
            <div class="row our_bakery_image">
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/dining01.jpg" alt="">
                </div>
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/dining02.jpg" alt="">
                </div>
                <div class="col-lg-4 col-sm-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/dining03.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!--================End Our Bakery Area =================-->



    <!--================Bakery Video Area =================-->
    <section class="bakery_video_area">
        <div class="container">
            <div class="video_inner">
                <h3>Gazania Cafe Interior</h3>
                <p>A light, sour wheat dough with roasted walnuts and freshly picked rosemary, thyme, poppy seeds, parsley and sage</p>
                <div class="media">
                    <div class="d-flex">
                        <a class="popup-youtube" href="http://www.youtube.com/watch?v=awKp3zpVM50"><i class="flaticon-play-button"></i></a>
                    </div>
                    <div class="media-body">
                        <h5>Watch intro video <br />about us</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Bakery Video Area =================-->


    <!--================Latest News Area =================-->
    <section class="latest_news_area gray_bg p_100">
        <div class="container">
            <div class="main_title">
                <h2>Latest Blog</h2>
                <h5>an turn into your instructor your helper, your </h5>
            </div>
            <div class="row latest_news_inner">
                <div class="col-lg-4 col-md-6">
                    <div class="l_news_image">
                        <div class="l_news_img">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/blog/latest-news/l-news-1.jpg" alt="">
                        </div>
                        <div class="l_news_hover">
                            <a href="#">
                                <h5>Oct 15, 2016</h5>
                            </a>
                            <a href="#">
                                <h4>Nanotechnology immersion along the information</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="l_news_item">
                        <div class="l_news_img">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/blog/latest-news/l-news-2.jpg" alt="">
                        </div>
                        <div class="l_news_text">
                            <a href="#">
                                <h5>Oct 15, 2016</h5>
                            </a>
                            <a href="#">
                                <h4>Nanotechnology immersion along the information</h4>
                            </a>
                            <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion ....</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="l_news_item">
                        <div class="l_news_img">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/blog/latest-news/l-news-3.jpg" alt="">
                        </div>
                        <div class="l_news_text">
                            <a href="#">
                                <h5>Oct 15, 2016</h5>
                            </a>
                            <a href="#">
                                <h4>Nanotechnology immersion along the information</h4>
                            </a>
                            <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion ....</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>