<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/fav-icon.png" type="image/x-icon" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?> </title>

    <!-- Icon css link -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/linearicons/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/flat-icon/flaticon.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/revolution/css/navigation.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/animate-css/animate.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">

    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/mousehover.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">

    <?php wp_head(); ?>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body <?php body_class( '' ); ?>>
    <a href="https://api.whatsapp.com/send?phone=971551851570&amp;text=Hi%20there%20,%20I%20have%20an%20enquiry" class="floats" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
    <!--================Main Header Area =================-->
    <header class="main_header_three">

        <div class="top_header_area row m0">
            <div class="container">
                <div class="float-left">
                    <a href="tell:0971551851570"><i class="fa fa-phone" aria-hidden="true"></i> 05 5185 1570</a>
                    <a href="mainto:info@cakebakery.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@cakebakery.com</a>
                </div>
                <div class="float-right">
                    <ul class="h_social list_style">
                        <li><a href="https://www.facebook.com/gazaniacafe/"><i class="fa fa-facebook"></i></a></li>
                        
                        <li><a href="https://www.instagram.com/gazaniacafe/"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                    <ul class="h_search list_style">


                        <?php echo do_shortcode("[woo_cart_but]"); ?>
                        <li class="shop_cart"><a href="<?php echo wc_get_cart_url(); ?>"><i class="lnr lnr-cart"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="top_logo_header">
            <div class="container">
                <div class="header_logo_inner">
                    <div class="h_left_text">
                        <div class="media">
                            <div class="d-flex">
                                <i class="flaticon-auricular-phone-symbol-in-a-circle"></i>
                            </div>
                            <div class="media-body table">
                                <a href="tel:097142231232" class="table-cell">(04) 223 1232</a>
                            </div>
                        </div>
                    </div>
                    <div class="h_middle_text">
                        <a href="<?php echo home_url() ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
                    </div>
                    <div class="h_right_text">
                        <a class="pink_btn" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ) ?>">Order online</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="middle_menu_three">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="my_toggle_menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    
                    <?php
                        echo wp_nav_menu(array(
                            'theme_location' => 'primary_menu',
                            'menu_class' => 'navbar-nav', 
                            'container' => false,
                            'fallback_cb'    => false
                        ) ); ?>


                    </div>
                </nav>
            </div>
        </div>
    </header>
    