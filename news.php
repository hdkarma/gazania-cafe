<?php /* Template Name: News page */ ?>

<?php get_header() ?>

    <section class="banner_area">
        <div class="container">
            <div class="banner_text">
                <h3>News</h3>
                <ul>
                    <li><a href="<?php echo home_url() ?>">Home</a></li>
                    <li><a href="<?php echo the_permalink(); ?>">News</a></li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Main Header Area =================-->



    <!--================Latest News Area =================-->
    <section class="latest_news_area gray_bg p_100">
        <div class="container">
            <div class="main_title">
                <h2>Latest Blog</h2>
                <h5>an turn into your instructor your helper, your </h5>
            </div>
            <div class="row latest_news_inner">

                <?php 
                    $flag = true;
                    $query = new WP_Query( array('post_type' => 'post', 'post_limits' => 1) ); if($query->have_posts()): ?>
                    <?php while($query->have_posts()): $query->the_post(); 
                        if($flag) : $flag = false; ?>
                        
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_image">
                                <div class="l_news_img">
                                    <?php echo the_post_thumbnail('shop_single', array('class' => 'img-fluid')); ?>
                                </div>
                                <div class="l_news_hover">
                                    <a href="<?php echo the_permalink(); ?>">
                                        <h5><?php echo the_date( 'l, F j, Y'); ?></h5>
                                    </a>
                                    <a href="<?php echo the_permalink(); ?>">
                                        <h4><?php echo the_title(); ?></h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php else: ?>
                            <div class="col-lg-4 col-md-6">
                                <div class="l_news_item">
                                    <div class="l_news_img">
                                        <?php echo the_post_thumbnail('shop_single', array('class' => 'img-fluid')); ?>
                                    </div>
                                    <div class="l_news_text">
                                        <a href="<?php echo the_permalink(); ?>">
                                            <h5><?php echo the_date( 'l, F j, Y'); ?></h5>
                                        </a>
                                        <a href="<?php echo the_permalink(); ?>">
                                            <h4><?php echo the_title(); ?></h4>
                                        </a>
                                        <p><?php echo the_excerpt(); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endif; endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>


    <?php get_footer() ?>
