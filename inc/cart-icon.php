<?php  // <~ don't add me in

add_shortcode ('woo_cart_but', 'woo_cart_but' );
/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
	ob_start();
 
        $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
  
        if ( $cart_count > 0 ) {
       ?>
           <style>
            .top_header_area .float-right .h_search li.shop_cart a:before{
                content: "<?php echo $cart_count; ?>";
            }
        </style>
        <?php
        }
	        
    return ob_get_clean();
 
}



add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count( $fragments ) {
 
    ob_start();
    
    $cart_count = WC()->cart->cart_contents_count;
    $cart_url = wc_get_cart_url();

    error_log(print_r($cart_count, true));
    
    ?>

	<?php
    if ( $cart_count > 0 ) {
        ?>
        <style>
            .top_header_area .float-right .h_search li.shop_cart a:before{
                content: "<?php echo $cart_count; ?>";
            }
        </style>
        <?php            
    }
        ?>
    <?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}


add_filter( 'wp_nav_menu_primary_menu_items', 'woo_cart_but_icon', 10, 2 ); // Change menu to suit - example uses 'primary_menu'

/**
 * Add WooCommerce Cart Menu Item Shortcode to particular menu
 */
function woo_cart_but_icon ( $items, $args ) {
       $items .=  '[woo_cart_but]'; // Adding the created Icon via the shortcode already created
       
       return $items;
}