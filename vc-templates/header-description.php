<?php 


// Element Class 
class vcHeaderFooter extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_products_title', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Heading and description.', 'text-domain'),
                'base' => 'vc_products_title',
                'description' => __('Heading and description block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(   
                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),     
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),     
                    array(
                        'type' => 'checkbox',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'This section have no bottom padding', 'text-domain' ),
                        'param_name' => 'nopadding',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),     
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {
         

         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'nopadding'   => false,
                ), 
                $atts
            )
        );

        $style = $nopadding ? "margin-bottom: 0px" : "";
        $html = '
            <section class="price_list_area pt_50" >
                <div class="container">
                    <div class="price_list_inner">
                        <div class="single_pest_title" style="'.$style .'">
                                <h2>'.$title.'</h2>
                                <p>'.$content.'</p>
                        </div>
                    </div>
                </div>
            </section>';
        return $html;
         
    }
     
} 
new vcHeaderFooter();  

?>