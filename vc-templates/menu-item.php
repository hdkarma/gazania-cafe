<?php 


// Element Class 
class vcMenuItem extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_menu_item', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Menu Item.', 'text-domain'),
                'base' => 'vc_menu_item',
                'description' => __('Menu Item block.', 'text-domain'), 
                'category' => __('Menu', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),  
                    array(
                        'type' => 'param_group',
                        'param_name' => 'menu',
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Content', 'text-domain' ),
                                'param_name' => 'desc',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Price', 'text-domain' ),
                                'param_name' => 'price',
                                'admin_label' => false,
                                'weight' => 0,
                            )   
                        )
                    ),
                          
                      
                )
            )
        );                                
            
    }

    public function getValue( $arr, $key ) {
        return array_key_exists($key, $arr) ? $arr[$key] : "";
    }

    public function vc_html( $atts ) {
         
         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => 'Menu',
                ), 
                $atts
            )
        );
        $menus = vc_param_group_parse_atts($atts['menu']);

            $html = '
            <div class="p_50">
                <div class="container">
                    <div class="price_list_inner">
                        <div class="single_pest_title">
                                <h2>'.$title.'</h2>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                <div class="discover_item_inner">
                                    <div class="row">';
                                        foreach($menus as $item){
                                            if($this->getValue($item, 'desc')){
                                                $html .= '
                                                <div class="col-lg-6">
                                                    <div class="discover_item">
                                                        <h4>'.$this->getValue($item, 'title').'</h4>
                                                        <p>'.$this->getValue($item, 'desc').'<span><span class="aed_curr">AED</span>'.$this->getValue($item, 'price').'</span></p>
                                                    </div>
                                                </div>';
                                            }else{
                                                $html .= '
                                                <div class="col-lg-6">
                                                    <div class="discover_item">
                                                        <h4>'.$this->getValue($item, 'title').'<p style="display: inline-flex;float:right"><span><span class="aed_curr">AED</span><span class="aed_price">'.$this->getValue($item, 'price').'</span></span></p></h4>
                                                    </div>
                                                </div>';
                                            }
                                            
                                        }
                                        $html .= '
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        return $html;
         
    }
     
} 
new vcMenuItem();  

?>