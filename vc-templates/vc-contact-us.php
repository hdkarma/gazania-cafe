<?php 


// Element Class 
class vcContactUs extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_contactus', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
        vc_map( 
            array(
                'name' => __('Contact us.', 'text-domain'),
                'base' => 'vc_contactus',
                'description' => __('Contact us block.', 'text-domain'), 
                'category' => __('Home', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Header',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'desc',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Header',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Form', 'text-domain' ),
                        'param_name' => 'form',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Form',
                    ),
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Form',
                    ),
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {


        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => 'Get in touch',
                    'desc'   => '',
                    'form'   => '[contact-form-7 id="66" title="Contact us page form"]',
                ), 
                $atts
            )
        );

        $html = '<section class="contact_form_area p_100">
        <div class="container">
            <div class="main_title">
                <h2>'.$title.'</h2>
                <h5>'.$desc.'</h5>
            </div>
            <div class="row">
                <div class="col-lg-7">'.do_shortcode($form).'</div>
                <div class="col-lg-4 offset-md-1">
                    <div class="contact_details">
                        '.$content.'
                    </div>
                </div>
            </div>
        </div>
    </section>';
        return $html;
    }
     
} 
new vcContactUs();  

?>