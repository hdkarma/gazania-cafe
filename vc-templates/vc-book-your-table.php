<?php 


// Element Class 
class vcBookYourTable extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_booktable', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
        vc_map( 
            array(
                'name' => __('Book your table.', 'text-domain'),
                'base' => 'vc_booktable',
                'description' => __('Book your table block.', 'text-domain'), 
                'category' => __('Home', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Form', 'text-domain' ),
                        'param_name' => 'form',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Header',
                    ),
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {


        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'form'   => '[contact-form-7 id="139" title="Book your table"]',
                ), 
                $atts
            )
        );

        $html = '<section class="order_now_area pt_100">
        <div class="container">
            <div class="order_now_inner">';
            $html .= do_shortcode($form).'
            </div>
        </div>
    </section>';
        return $html;
    }
     
} 
new vcBookYourTable();  

?>