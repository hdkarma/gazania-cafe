<?php 


// Element Class 
class vcCakesRounded extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_cakes_rounded', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Cakes rounded block.', 'text-domain'),
                'base' => 'vc_cakes_rounded',
                'description' => __('Cakes rounded block block.', 'text-domain'), 
                'category' => __('offers', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'param_group',
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'heading' => __( 'Image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'heading' => __( 'Link', 'text-domain' ),
                                'param_name' => 'link',
                                'admin_label' => false,
                                'weight' => 0,
                            )
                        )
                    ),
                          
                      
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {
         

        $items = vc_param_group_parse_atts($atts['items']);


        $html = '
        <section class="service_offer_area p_100">
            <div class="container">
                <div class="row service_main_item_inner">
                <div class="main_title">
                    <p>'.$content.'</p>
                </div>
                ';
        foreach($items as $item){
            $img = wp_get_attachment_image_src($item["image"], "large")[0];

            $html .= '
                    <div class="col-lg-4 col-sm-6">
                        <div class="service_m_item">
                            <div class="service_img_inner">
                                <div class="service_img">
                                    <img class="rounded-circle" src="'.$img.'" alt="">
                                </div>
                            </div>
                            <div class="service_text">
                                <a href="'.$item["link"].'">
                                    <h4>'.$item["title"].'</h4>
                                </a>
                            </div>
                        </div>
                    </div>';
        }
        $html .= '
            </div>
        </div>
        </section>';

        return $html;
         
    }
     
} 
new vcCakesRounded();  

?>