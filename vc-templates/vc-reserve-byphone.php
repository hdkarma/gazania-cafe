<?php 


// Element Class 
class vcReserveByPhone extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_reserve_byphone', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
        vc_map( 
            array(
                'name' => __('Reserve By phone.', 'text-domain'),
                'base' => 'vc_reserve_byphone',
                'description' => __('Reserve By phone block.', 'text-domain'), 
                'category' => __('Home', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {


        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                ), 
                $atts
            )
        );

        $html = '<div class="container">
        <div class="row">
            <div class="r-phone">
                <h3>'.$title.'</h3>
                <p> '.$content.'</p>
            </div>
        </div>
    </div>';
        return $html;
    }
     
} 
new vcReserveByPhone();  

?>