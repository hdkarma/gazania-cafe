<?php 


// Element Class 
class vcLatestBlog extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_latest_blog', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Latest blog.', 'text-domain'),
                'base' => 'vc_latest_blog',
                'description' => __('Latest blog block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(   
                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),     
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),     
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {
         

         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => '',
                ), 
                $atts
            )
        );

        $html = '
            <section class="latest_news_area gray_bg p_100">
        <div class="container">
            <div class="main_title">
                <h2>'.$title.'</h2>
                <h5>'.$content.' </h5>
            </div>
            <div class="row latest_news_inner">';

                    $flag = true;
                    $query = new WP_Query( array('post_type' => 'post', 'post_limits' => 1) ); if($query->have_posts()):
                    while($query->have_posts()): $query->the_post(); 
                        if($flag) : $flag = false; 
                        $html .= '
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_image">
                                <div class="l_news_img">
                                    '.get_the_post_thumbnail(get_the_ID(), 'shop_single', array('class' => 'img-fluid')).'
                                </div>
                                <div class="l_news_hover">
                                    <a href="'.get_the_permalink().'">
                                        <h5>'.get_the_date( 'l, F j, Y').'</h5>
                                    </a>
                                    <a href="'.get_the_permalink().'">
                                        <h4>'.get_the_title().'</h4>
                                    </a>
                                </div>
                            </div>
                        </div>';
                        else: 
                            $html .= '
                            <div class="col-lg-4 col-md-6">
                                <div class="l_news_item">
                                    <div class="l_news_img">
                                        '.get_the_post_thumbnail(get_the_ID(), 'shop_single', array('class' => 'img-fluid')).'
                                    </div>
                                    <div class="l_news_text">
                                        <a href="'.get_the_permalink().'">
                                            <h5>'.get_the_date( 'l, F j, Y').'</h5>
                                        </a>
                                        <a href="'.get_the_permalink().'">
                                            <h4>'.get_the_title().'</h4>
                                        </a>
                                        <p>'.get_the_excerpt().'</p>
                                    </div>
                                </div>
                            </div>';
                         endif; endwhile;
                 endif;
                 $html .= '
            </div>
        </div>
    </section>
';
        return $html;
         
    }
     
} 
new vcLatestBlog();  

?>