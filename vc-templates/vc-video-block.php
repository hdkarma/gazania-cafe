<?php 


// Element Class 
class vcVideo extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_video', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Video.', 'text-domain'),
                'base' => 'vc_video',
                'description' => __('Video block.', 'text-domain'), 
                'category' => __('Video', 'text-domain'),            
                'params' => array(   

                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),     
                    array(
                        'type' => 'textarea',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'desc',
                        'admin_label' => false,
                        'weight' => 0,
                    ),     
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Youtube video link', 'text-domain' ),
                        'param_name' => 'youtube',
                        'admin_label' => false,
                        'weight' => 0,
                    ),     
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Youtube intro', 'text-domain' ),
                        'param_name' => 'youtube_intro',
                        'admin_label' => false,
                        'weight' => 0,
                    ),     
                    array(
                        'type' => 'attach_image',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Background Image', 'text-domain' ),
                        'param_name' => 'background',
                        'admin_label' => false,
                        'weight' => 0,
                    ) 
                ),
            )
        );                                
            
    }

    public function getValue( $arr, $key ) {
        return array_key_exists($key, $arr) ? $arr[$key] : "";
    }

    public function vc_html( $atts ) {
         

         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'desc'   => '',
                    'youtube'   => '',
                    'youtube_intro'   => '',
                    'background'   => '',
                ), 
                $atts
            )
        );

        $background_src = get_template_directory_uri(). '/img/video/video-1.jpg';

        if($background) $background_src = wp_get_attachment_image_src($background, "large")[0];

        $html = '
            <section class="bakery_video_area" style="background: url('.$background_src.') no-repeat scroll center center">
                <div class="container">
                    <div class="video_inner">
                        <h3>'.$title.'</h3>
                        <p>'.$desc.'</p>
                        <div class="media">
                            <div class="d-flex">
                                <a class="popup-youtube" href="'.$youtube.'"><i class="flaticon-play-button"></i></a>
                            </div>
                            <div class="media-body">
                                <h5>'.$youtube_intro.'</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </section>';
        return $html;
         
    }
     
} 
new vcVideo();  

?>