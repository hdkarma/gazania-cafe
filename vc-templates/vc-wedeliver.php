<?php 


// Element Class 
class vcWeDeliver extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_wedeliver', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('We deliver.', 'text-domain'),
                'base' => 'vc_wedeliver',
                'description' => __('We deliver block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(  
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Header',
                    ),     
                    array(
                        'type' => 'textarea',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Header',
                    ),     
                    array(
                        'type' => 'param_group',
                        'param_name' => 'items',
                        'group' => 'Content',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),  
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textarea',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Content', 'text-domain' ),
                                'param_name' => 'mcontent',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Button text', 'text-domain' ),
                                'param_name' => 'button_text',
                                'admin_label' => false,
                                'weight' => 0,
                            ), 
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Button link', 'text-domain' ),
                                'param_name' => 'button_link',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                        )
                    )
                )
            )
        );                                
            
    }

    public function vc_html( $atts ) {
         

         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'content'   => '',
                ), 
                $atts
            )
        );
        $items = vc_param_group_parse_atts($atts['items']);
        $html = '
        <section class="special_area p_100">
        <div class="container">
            <div class="main_title">
                <h2>'.$title.'</h2>
                <h5>'.$content.'</h5>
            </div>
            <div class="special_item_inner">';

            foreach($items as $key=>$item){
                $img = wp_get_attachment_image_src($item["image"], "large")[0];
                if($key%2==0){
                    $html .= '
                        <div class="specail_item">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="s_left_img">
                                        <img class="img-fluid" src="'.$img.'" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="special_item_text">
                                        <h4>'.$item["title"].'</h4>
                                        <p>'.$item["mcontent"].'</p>
                                        <a class="pink_btn" href="'.$item["button_link"].'">'.$item["button_text"].'</a>
                                    </div>
                                </div>
                            </div>
                        </div>';

                }else{
            $html .= '
                <div class="specail_item">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="s_item_left">
                                <div class="main_title">
                                    <h2>'.$item["title"].' </h2>
                                </div>

                                <p>'.$item["mcontent"].'</p>

                                <a class="pink_btn" href="'.$item["button_link"].'">'.$item["button_text"].'</a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="s_right_img">
                                <img class="img-fluid" src="'.$img.'" alt="">
                            </div>
                        </div>
                    </div>
                </div>';
            }
            }
            $html .='
            </div>
        </div>
    </section>';
        return $html;
         
    }
     
} 
new vcWeDeliver();  

?>