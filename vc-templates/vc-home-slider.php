<?php 


// Element Class 
class vcHomeSlider extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_home_slider', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Home Slider.', 'text-domain'),
                'base' => 'vc_home_slider',
                'description' => __('Home Slider block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(   
                
                    array(
                        'type' => 'param_group',
                        'param_name' => 'slides',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'heading' => __( 'Attach image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textarea',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Description', 'text-domain' ),
                                'param_name' => 'desc',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Button text', 'text-domain' ),
                                'param_name' => 'button_text',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Button Link', 'text-domain' ),
                                'param_name' => 'button_link',
                                'admin_label' => false,
                                'weight' => 0,
                            )
                        )
                    )
                )
            )
        );                             
            
    }

    public function vc_html( $atts, $content = null ) {
         

        $slides = vc_param_group_parse_atts($atts['slides']);


            $html = '
            <section class="main_slider_area">
        <div id="main_slider3" class="rev_slider" data-version="5.3.1.6">
            <ul>';
            foreach($slides as $slide){
                $img = wp_get_attachment_image_src($slide["image"], "full")[0];
                $html .= '
                <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="'.$img.'" data-rotate="0" data-saveperformance="off" data-title="'.$slide["title"].'" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="'.$img.'" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <div class="slider_text_box">
                        <div class="tp-caption tp-resizeme first_text black" data-x="[\'left\',\'left\',\'left\',\'15\',\'15\']" data-hoffset="[\'0\',\'15\',\'15\',\'0\']" data-y="[\'top\',\'top\',\'top\',\'top\']" data-voffset="[\'220\',\'220\',\'170\',\'170\',\'130\']" data-fontsize="[\'65\',\'65\',\'65\',\'40\',\'30\']" data-lineheight="[\'80\',\'80\',\'80\',\'50\',\'40\']" data-width="[\'800\',\'800\',\'800\',\'400\']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="[\'left\',\'left\',\'left\',\'left\']">'.$slide["title"].'</div>
                        <div class="tp-caption tp-resizeme secand_text black" data-x="[\'left\',\'left\',\'left\',\'15\',\'15\']" data-hoffset="[\'0\',\'15\',\'15\',\'0\']" data-y="[\'top\',\'top\',\'top\',\'top\']" data-voffset="[\'388\',\'388\',\'340\',\'290\',\'225\']" data-fontsize="[\'20\',\'20\',\'20\',\'20\',\'16\']" data-lineheight="[\'28\',\'28\',\'28\',\'28\']" data-width="[\'640\',\'640\',\'640\',\'640\',\'350\']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">'.$slide["desc"].'
                        </div>
                        <div class="tp-caption tp-resizeme slider_button" data-x="[\'left\',\'left\',\'left\',\'15\',\'15\']" data-hoffset="[\'0\',\'15\',\'15\',\'0\']" data-y="[\'top\',\'top\',\'top\',\'top\']" data-voffset="[\'485\',\'485\',\'430\',\'390\',\'330\']" data-fontsize="[\'14\',\'14\',\'14\',\'14\']" data-lineheight="[\'46\',\'46\',\'46\',\'46\']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                            <a class="now_btn" href="'.$slide["button_link"].'">'.$slide["button_text"].'</a>
                        </div>
                    </div>
                </li>';
            }
            $html .= '
                
            </ul>
        </div>
    </section>';
        return $html;
         
    }
     
} 
new vcHomeSlider();  

?>