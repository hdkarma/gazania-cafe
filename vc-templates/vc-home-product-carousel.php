<?php 


// Element Class 
class vcHomeCarousel extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_home_carousel', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Home Carousel.', 'text-domain'),
                'base' => 'vc_home_carousel',
                'description' => __('Home Carousel block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h3',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'desc',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                )
            )
        );                             
            
    }

    public function vc_html( $atts, $content = null ) {
        extract(
            shortcode_atts(
                array(
                    'title'   => 'Our Featured Cakes',
                    'desc'   => '',
                ), 
                $atts
            )
        );



            $html = '
            <section class="welcome_bakery_area pink_cake_feature">
                <div class="container">
                    <div class="cake_feature_inner">
                        <div class="title_view_all">
                            <div class="float-left">
                                <div class="main_w_title">
                                    <h2>'.$title.'</h2>
                                    <h5>'.$desc.'</h5>
                                </div>
                            </div>
                            <div class="float-right">
                                <a class="pest_btn" href="#">View all Products</a>
                            </div>
                        </div>
                        <div class="cake_feature_slider owl-carousel">';

                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 6
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                            $html .= '
                            <div class="item">
                                <div class="cake_feature_item">
                                    <div class="cake_img">
                                        '.$product->get_image("shop-thumb").'
                                    </div>
                                <div class="cake_text">
                                    <h4>AED'.$product->get_price().'</h4>
                                    <h3>'.get_the_title().'</h3>
                                    <a class="pest_btn" href="'.get_the_permalink().'">View product</a>
                                </div>
                                </div>
                                
                            </div>';
                            
                        endwhile;
                        }
                        wp_reset_postdata();
                    $html .= '
                    </div>
                </div>
            </section>';
        return $html;
         
    }
     
} 
new vcHomeCarousel();  

?>