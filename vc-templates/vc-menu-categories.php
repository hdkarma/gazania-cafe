<?php 


// Element Class 
class vcMenuCategories extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_menu_categories', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Menu Categories.', 'text-domain'),
                'base' => 'vc_menu_categories',
                'description' => __('Menu Categories block.', 'text-domain'), 
                'category' => __('Header', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h3',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'desc',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'param_group',
                        'param_name' => 'categories',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'heading' => __( 'Attach image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Link', 'text-domain' ),
                                'param_name' => 'link',
                                'admin_label' => false,
                                'weight' => 0,
                            )
                        )
                    )
                )
            )
        );                             
            
    }

    public function vc_html( $atts, $content = null ) {
        extract(
            shortcode_atts(
                array(
                    'title'   => 'OUR MENU',
                    'desc'   => '',
                ), 
                $atts
            )
        );

        $categories = vc_param_group_parse_atts($atts['categories']);


            $html = '
            <section class="our_bakery_area p_100">
                <div class="container">
                    <div class="our_bakery_text">
                        <h2>'.$title.'</h2>
                        <h6>'.$desc.'</h6>
                    </div>
                    <div class="row our_bakery_image">';
                    foreach($categories as $category){
                        $img = wp_get_attachment_image_src($category["image"], "large")[0];
                        $html .= '
                        <!-- product starting -->
                        <div class="col-lg-4 col-sm-6 mb-40">
                            <figure class="snip1529">
                                <img src="'.$img.'" alt="'.$category['title'].'" />
                                <div class="menu"><span class="menu-bold">MENU</span></div>
                                <figcaption>
                                    <h3>'.$category['title'].'</h3>
                                </figcaption>
                                <div class="hover"><i class="ion-android-open"></i></div>
                                <a href="'.$category['link'].'"></a>
                            </figure>

                        </div>';
                    }
                    $html .= '
                    </div>
                </div>
            </section>';
        return $html;
         
    }
     
} 
new vcMenuCategories();  

?>