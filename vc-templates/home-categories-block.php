<?php 


// Element Class 
class vcHomeCategories extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_home_categories', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Home Categories.', 'text-domain'),
                'base' => 'vc_home_categories',
                'description' => __('Home Categories block.', 'text-domain'), 
                'category' => __('Home', 'text-domain'),            
                'params' => array(   
                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Title',
                    ),     
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Title',
                    ),     
                    array(
                        'type' => 'param_group',
                        'param_name' => 'categories',
                        'group' => 'Content',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Title', 'text-domain' ),
                                'param_name' => 'title',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Link', 'text-domain' ),
                                'param_name' => 'link',
                                'admin_label' => false,
                                'weight' => 0,
                            )
                        )
                    ),
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {
         

         // Params extraction
         extract(
            shortcode_atts(
                array(
                    'title'   => '',
                ), 
                $atts
            )
        );
        $menus = vc_param_group_parse_atts($atts['menus']);


            $html = '
            <section class="our_bakery_area p_100">
                <div class="container">
                    <div class="our_bakery_text">
                        <h2>'.$title.'</h2>
                        <h6>'.$content.'</h6>
                    </div>
                    <div class="row our_bakery_image">';
                    foreach($menus as $item){
                        if($this->getValue($item, 'desc')){
                            $img = wp_get_attachment_image_src($item["image"], "large")[0];
                            $html .= '
                            <div class="col-lg-4 col-sm-6 mb-40">
                                <figure class="snip1529">
                                    <img src="'.$img.'" alt="Gazania Cafe" />
                                    <div class="menu"><span class="menu-bold">MENU</span></div>
                                    <figcaption>
                                        <h3>'.$item['title'].'</h3>
                                    </figcaption>
                                    <div class="hover"><i class="ion-android-open"></i></div>
                                    <a href="'.$item['link'].'"></a>
                                </figure>
                            </div>';
                        }
                        
                    }
                    $html .= '
                        
                    </div>
                </div>
            </section>';
        return $html;
         
    }
     
} 
new vcHomeCategories();  

?>