<?php 


// Element Class 
class vcOfferBlock extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_three_offerblock', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
         

        vc_map( 
      
            array(
                'name' => __('Three images.', 'text-domain'),
                'base' => 'vc_three_offerblock',
                'description' => __('Three images  block.', 'text-domain'), 
                'category' => __('offers', 'text-domain'),            
                'params' => array(   

                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Class', 'text-domain' ),
                        'param_name' => 'class',
                        'admin_label' => false,
                        'weight' => 0,
                    ),    

                    array(
                        'type' => 'param_group',
                        'param_name' => 'offers',
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Image', 'text-domain' ),
                                'param_name' => 'image',
                                'admin_label' => false,
                                'weight' => 0,
                            ),     
                            array(
                                'type' => 'textfield',
                                'holder' => 'h3',
                                'class' => 'title-class',
                                'heading' => __( 'Link', 'text-domain' ),
                                'param_name' => 'link',
                                'admin_label' => false,
                                'weight' => 0,
                            )
                        )
                    ),
                          
                      
                )
            )
        );                                
            
    }

    public function vc_html( $atts ) {
         

        $offers = vc_param_group_parse_atts($atts['offers']);

        
        extract(
            shortcode_atts(
                array(
                    'class'   => 'p_50',
                ), 
                $atts
            )
        );


            $html = '
            <div class="'.$class.'">
                <div class="container">
                    <div class="row our_bakery_imag">';
                            foreach($offers as $item){
                                $img = wp_get_attachment_image_src($item["image"], "large")[0];
                                $html .= '
                                <div class="col-md-4 col-6"><a href="'.$item["link"].'"><img class="img-fluid offer-img" src="'.$img.'" alt=""></a></div>';
                            }
                            $html .= '
                    </div>
                </div>
            </div>';
        return $html;
         
    }
     
} 
new vcOfferBlock();  

?>