<?php 


// Element Class 
class vcBreadcrumbs extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_about_rows_mapping' ) );
        add_shortcode( 'vc_breadcrumbs', array( $this, 'vc_html' ) );
    }
     

 
    public function vc_about_rows_mapping() {
        vc_map( 
            array(
                'name' => __('Breadcrumbs.', 'text-domain'),
                'base' => 'vc_breadcrumbs',
                'description' => __('Breadcrumbs block.', 'text-domain'), 
                'category' => __('Home', 'text-domain'),            
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Title',
                    ),
                )
            )
        );                                
            
    }

    public function vc_html( $atts, $content = null ) {

        $default_title = get_the_title(  );

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => $default_title,
                ), 
                $atts
            )
        );

        $html = '<section class="banner_area"><div class="container"><div class="banner_text"><h3>'.$title.'</h3><ul>';
        $html .= '<li><a href="' . home_url() . '">Home</a></li>';
        $html .= '<li><a href="#">'.$title.'</a></li>';
        $html .= '</ul></div></div></section>';
        return $html;
    }
     
} 
new vcBreadcrumbs();  

?>