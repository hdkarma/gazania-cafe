<?php


require_once( get_template_directory().'/inc/cart-icon.php' );  

if( defined( 'WPB_VC_VERSION' ) ) {
  
    require_once( get_template_directory().'/vc-templates/header-description.php' );  
    require_once( get_template_directory().'/vc-templates/menu-item.php' );  
    require_once( get_template_directory().'/vc-templates/offer-three-block.php' );  
    require_once( get_template_directory().'/vc-templates/home-categories-block.php' );  
    require_once( get_template_directory().'/vc-templates/vc-breadcrumbs.php' );  
    require_once( get_template_directory().'/vc-templates/vc-contact-us.php' );  
    require_once( get_template_directory().'/vc-templates/vc-video-block.php' );  

    require_once( get_template_directory().'/vc-templates/vc-book-your-table.php' );  
    require_once( get_template_directory().'/vc-templates/vc-reserve-byphone.php' );  

    require_once( get_template_directory().'/vc-templates/vc-home-slider.php' );  
    require_once( get_template_directory().'/vc-templates/vc-menu-categories.php' );  
    require_once( get_template_directory().'/vc-templates/vc-home-product-carousel.php' );  
    require_once( get_template_directory().'/vc-templates/vc-latest-blog.php' );  
    
    require_once( get_template_directory().'/vc-templates/vc-wedeliver.php' );  
    require_once( get_template_directory().'/vc-templates/vc-products-rounded.php' );  

    
	// require_once( get_template_directory().'/vc-templates/vc-about-row.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-careers-block.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-contact-block.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-megaplant-block.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-ourproducts-rounded.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-ourproducts-fullslider.php' ); 
  
	// require_once( get_template_directory().'/vc-templates/vc-timeline-block.php' );  
	// require_once( get_template_directory().'/vc-templates/vc-mission-vision.php' );  
  
  }

  function mytheme_add_themesupport() {
 
   register_nav_menus( array(
        'primary_menu' => 'Primary Navigation',
        'quicklinks_menu' => 'Quick links',
    ) );
}
add_action( 'init', 'mytheme_add_themesupport' );


function wpdocs_custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_theme_support( 'woocommerce' );



	add_image_size( 'shop-thumb', 270, 226 );
    add_image_size( 'sidebar-thumb', 104, 95 );
}

add_filter('wp_list_categories', 'cat_count_span');
function cat_count_span($links) {
  $links = str_replace('</a> (', '</a> <span>(', $links);
  $links = str_replace(')', ')</span>', $links);
  return $links;
}

add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {
    /* Register the 'shop' sidebar. */
    register_sidebar(
        array(
            'id'            => 'shop',
            'name'          => __( 'Shop Sidebar' ),
            'description'   => __( 'A short description of the Shop.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');

function wc_customize_product_sorting($sorting_options){
    $sorting_options = array(
        'menu_order' => __( 'Sorting', 'woocommerce' ),
        'popularity' => __( 'popularity', 'woocommerce' ),
        'rating'     => __( 'average rating', 'woocommerce' ),
        'date'       => __( 'newness', 'woocommerce' ),
        'price'      => __( 'price: low to high', 'woocommerce' ),
        'price-desc' => __( 'price: high to low', 'woocommerce' ),
    );

    return $sorting_options;
}

function so_38878702_remove_hook(){
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
 }
 add_action( 'woocommerce_before_shop_loop', 'so_38878702_remove_hook', 1 );


 // breadcrumbs
 add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => '',
            'wrap_before' => '<div class="breadcrumb-area mb-30"><div class="container-fluid"><div class="row"><div class="col-12"><div class="breadcrumb-wrap"><nav class="breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav></div></div></div></div></div>',
            'before'      => '<li class="breadcrumb-item">',
            'after'       => '</li>',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}
add_filter('woocommerce_currency_symbol', 'my_currency_symbol', 10, 2);
  
function my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'AED': $currency_symbol = 'AED'; 
	  break;
     }
     return $currency_symbol;
}