<?php /* Template Name: Primary page template */ ?>

<?php get_header() ?>
    <div class="container">
    <?php while ( have_posts() ) : the_post(); ?>
        <?php 	echo the_content(); ?>
    <?php endwhile; ?>
    </div>
<?php get_footer() ?>