

    <!--================Footer Area =================-->
    <footer class="footer_area">
        <div class="footer_widgets">
            <div class="container">
                <div class="row footer_wd_inner">
                    <div class="col-lg-3 col-6">
                        <aside class="f_widget f_about_widget">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="" class="d-block img-fluid">
                            <p>The Gazania flower is a treasure from the Ancient Greek and blows under the Sun. Gazania Café takes you to a delicious journey of beauty, color and warmth and offers you a bloom of tastes, a fusion between local Emirati dishes, Moroccan tagines to healthy and unique creations.</p>
                            <ul class="nav">
                                <li><a href="https://www.facebook.com/gazaniacafe/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/gazaniacafe/"><i class="fa fa-linkedin"></i></a></li>
                                        
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-3 col-6">
                        <aside class="f_widget f_link_widget">
                            <div class="f_title">
                                <h3>Quick links</h3>
                            </div>

                            <?php
                                echo wp_nav_menu(array(
                                    'theme_location' => 'quicklinks_menu',
                                    'menu_class' => 'list_style', 
                                    'container' => false,
                                    'fallback_cb'    => false
                                ) ); ?>
                        </aside>
                    </div>
                    <div class="col-lg-3 col-6">
                        <aside class="f_widget f_link_widget">
                            <div class="f_title">
                                <h3>Work Times</h3>
                            </div>
                            <ul class="list_style">
                                <li><a href="#">Mon. : Fri.: 8 am - 8 pm</a></li>
                                <li><a href="#">Sat. : 9am - 4pm</a></li>
                                <li><a href="#">Sun. : Closed</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-3 col-6">
                        <aside class="f_widget f_contact_widget">
                            <div class="f_title">
                                <h3>Contact Info</h3>
                            </div>
                            <h4>(04)223 1232, 055 172 2000</h4>
                            <p>Dubai - United Arab Emirates</p>
                            <h5>info@gazaniacafe.com</h5>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_copyright">
            <div class="container">
                <div class="copyright_inner">
                    <div class="float-left">
                        <h5>© Copyright cakebakery Reidis. All right reserved.</h5>
                    </div>
                    <div class="float-right">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/payment-method.png" alt="gazaniacafe">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================End Footer Area =================-->






    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <!-- Rev slider js -->
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <!-- Extra plugin js -->
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/datetime-picker/js/moment.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/nice-select/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vendors/lightbox/simpleLightbox.min.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/theme.js"></script>
    
    
    <?php wp_footer(); ?>
</body>



</html>

